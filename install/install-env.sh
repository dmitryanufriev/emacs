#!/usr/bin/env bash

LGREEN_COLOR='\033[1;32m'
NO_COLOR='\033[0m'

function install-additional-packages()
{
    {
        printf "Install silversearcher-ag..."        
        if ! dpkg -s silversearcher-ag >/dev/null 2>&1; then
	    sudo apt-get install -y silversearcher-ag > /dev/null && printf "OK!\n"	
        else
	    printf "already installed.\n"
        fi
    } && { 
        printf "Install build-essential..."
        if ! dpkg -s build-essential >/dev/null 2>&1; then
	    sudo apt-get install -y build-essential > /dev/null && printf "OK!\n"		
        else
	    printf "already installed.\n"	
        fi
    }

    printf "Install Exuberant Ctags..."
    if ! dpkg -s exuberant-ctags >/dev/null 2>&1; then
	sudo apt-get install -y exuberant-ctags > /dev/null && printf "OK!\n"
    else
	printf "already installed.\n"
    fi
}

function create-emacs-d()
{
    SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
    cd $SCRIPT_DIR/../python
    printf "Create $HOME/.emacs.d directory..."    
    ln -s $(pwd) $HOME/.emacs.d
    printf "OK!\n"
    cd - > /dev/null
}

function install-emacs()
{
    sudo add-apt-repository -y ppa:ubuntu-elisp/ppa > /dev/null
    printf "Update Linux packages..." && sudo apt-get update > /dev/null && printf "OK!\n"

    if ! dpkg -s emacs-snapshot >/dev/null 2>&1; then
	printf "Install emacs-snapshot..." && sudo apt-get install -y emacs-snapshot > /dev/null && printf "OK!\n"
    else
	echo "already installed.\n"
    fi

    if ! dpkg -s emacs-snapshot-el >/dev/null 2>&1; then
	printf "Install emacs-snapshot-el..." && sudo apt-get install -y emacs-snapshot-el > /dev/null && printf "OK!\n"	
    else
	echo "already installed.\n"
    fi
}

function install-and-setup-cask()
{
    printf "Install cask..."    
    if ! which cask >/dev/null 2>&1; then
	if ! dpkg -s curl >/dev/null 2>&1; then
	    sudo apt-get install -y curl && printf "OK!\n"
	else
	    printf "alredy installed.\n"
	fi
	curl -fsSL https://raw.githubusercontent.com/cask/cask/master/go | python
	if ! grep .cask/bin $HOME/.profile >/dev/null 2>&1; then
	    printf "\n\n%s\n" 'export PATH="$HOME/.cask/bin:$PATH"' >> $HOME/.profile
	    printf "${LGREEN_COLOR}cask successfully added to PATH!${NO_COLOR}\n"
	    source ~/.profile
	else
	    echo 'cask already in PATH.'
	fi
    else
	printf "already installed.\n"
    fi
}

function install-javascript-environment()
{
    printf "Install nodejs and npm..."    
    if ! dpkg -s nodejs >/dev/null 2>&1; then
	curl -sL https://deb.nodesource.com/setup | sudo bash - > /dev/null
	sudo apt-get install -y nodejs > /dev/null
	printf "OK!\n"
    else
	printf "already installed.\n"
    fi

    printf "Install tern..."
    if ! which tern >/dev/null 2>&1; then
	sudo npm install -g tern
	printf "OK!\n"	
    else
	printf "already installed.\n"
    fi
}

function setup-python-environment()
{        
    printf "Install pip..."
    if ! which pip >/dev/null 2>&1; then        
        if [ -f ./get-pip.py ]; then
            rm ./get-pip.py
        fi
        
        echo "Get pip installation script..."
        echo "------------------------------"
        wget https://bootstrap.pypa.io/get-pip.py
	sudo python get-pip.py
	printf "OK!\n"
    else
	printf "already installed.\n"
    fi
    
    if [ -f ./get-pip.py ]; then
        rm ./get-pip.py
    fi

    printf "Install virtualenv..."
    if ! which virtualenv >/dev/null 2>&1; then
	sudo pip install virtualenv
	printf "OK!\n"	
	if [ ! -d "$HOME/.virtualenvs" ]; then
	    mkdir "$HOME/.virtualenvs"
	fi
    else
	printf "already installed.\n"
    fi
}

function setup-emacs-packages()
{
    printf "Install emacs packages..."
    cd $HOME/.emacs.d
    cask install
    cd - > /dev/null
    printf "OK!\n"        
}

if [ -d "$HOME/.emacs.d" ]; then
    echo "$HOME/.emacs.d already exists. Replace it (yes or no)?"
    read answer

    while [[ x$answer != xyes && x$answer != xno ]]
    do
	echo "You have entered an invalid response. Please try again..."
	echo "$HOME/.emacs.d already exists. Replace it (yes or no)?"
	read answer
    done

    if [[ x$answer == xyes ]]; then
	rm -rf $HOME/.emacs.d
    else
	echo "Installation stopped, because Emacs already installed."
	exit
    fi
fi

install-additional-packages && 
    create-emacs-d &&
    install-emacs &&
    install-and-setup-cask &&
    install-javascript-environment &&
    setup-python-environment &&
    setup-emacs-packages

printf "\n${LGREEN_COLOR}DONE!${NO_COLOR}\n\n"