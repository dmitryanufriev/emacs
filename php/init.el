;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'cask "~/.cask/cask.el")
(cask-initialize)

;; Indent whole buffer
(global-set-key (kbd "<f12>") 'indent-buffer)

(defun indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

;; Disable toolbar and scrollbar
(when (window-system)
  (scroll-bar-mode -1)
  (tool-bar-mode -1))

;; Allow change selected text
(delete-selection-mode t)

;; Highlight parens
(show-paren-mode t)

;; Disable backup files
(setq make-backup-file nil)

;; Disable autosave
(setq auto-save-default nil)

;; Display ido results vertically, rather than horizontally
(setq ido-decorations (quote ("\n-> " "" "\n   " "\n   ..." "[" "]" " [No match]" " [Matched]" " [Not readable]" " [Too big]" " [Confirm]")))
(defun ido-disable-line-truncation () (set (make-local-variable 'truncate-lines) nil))
(add-hook 'ido-minibuffer-setup-hook 'ido-disable-line-truncation)
(defun ido-define-keys () ;; C-n/p is more intuitive in vertical layout
  (define-key ido-completion-map (kbd "C-n") 'ido-next-match)
  (define-key ido-completion-map (kbd "C-p") 'ido-prev-match))
(add-hook 'ido-setup-hook 'ido-define-keys)

;; Enable IDO mode
(ido-mode t)
(ido-everywhere t)
(setq ido-use-faces nil)

;; Move between windows with SHIFT-arrow
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; Remove blank lines at the end of file/buffer
(defun delete-trailing-blank-lines ()
  "Deletes all blank lines at the end of the file, even the last one"
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-max))
      (delete-blank-lines)
      (let ((trailnewlines (abs (skip-chars-backward "\n\t"))))
	(if (> trailnewlines 0)
	    (progn
	      (delete-char trailnewlines)))))))
(add-hook 'before-save-hook 'delete-trailing-blank-lines)

;; *** PACKAGES ***


;; WHICH-KEY
(which-key-mode t)

;; YASNIPPET
(yas-global-mode 1)

;; WEB MODE
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))

(setq web-mode-ac-sources-alist
      '(("css" . (ac-source-css-property ac-source-words-in-buffer))
	("html" . (ac-source-words-in-buffer ac-source-abbrev))))

(setq web-mode-engines-alist
      '(("php" . "\\.html\\'")))

(defun my-web-mode-hook ()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 4)
  (setq web-mode-enable-current-column-highlight t)
  (nlinum-mode t)
  (emmet-mode t))
(add-hook 'web-mode-hook  'my-web-mode-hook)

(defun web-mode-css-hook()
  "Enable yasnippet for css"
  (let ((web-mode-cur-language
	 (web-mode-language-at-pos)))
    (if (string= web-mode-cur-language "css")
	(yas-activate-extra-mode 'css-mode)
      (yas-deactivate-extra-mode 'css-mode))))
(add-hook 'web-mode-hook  'web-mode-css-hook)

;; PHP MODE
(add-hook 'php-mode-hook
	  (lambda()
	    (auto-complete-mode t)
	    (require 'ac-php)
	    ;;(setq ac-php-use-cscope-flag  t ) ;;enable cscope
	    (setq ac-sources  '(ac-source-php ac-source-symbols ac-source-words-in-same-mode-buffers))
	    (setq ac-use-menu-map t)
	    (define-key ac-menu-map "\C-n" 'ac-next)
	    (define-key ac-menu-map "\C-p" 'ac-previous)
	    (define-key php-mode-map  (kbd "C-]") 'ac-php-find-symbol-at-point)
	    (define-key php-mode-map  (kbd "C-t") 'ac-php-location-stack-back)
	    (nlinum-mode t)
	    (local-set-key (kbd "RET") 'newline-and-indent)
	    (flymake-php-load)))

;; EMMET MODE
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wombat)))
 '(package-selected-packages (quote (package-build shut-up epl git commander f dash s))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )