(package-initialize)

(require 'cask "~/.cask/cask.el")
(cask-initialize)

;; *** CUSTOM SETTINGS/FUNCTIONS ***

;; Indent whole buffer
(global-set-key (kbd "<f12>") 'indent-buffer)

(defun indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

;; Disable toolbar and scrollbar
(when (window-system)
  (scroll-bar-mode -1)
  (tool-bar-mode -1))

;; Allow change selected text
(delete-selection-mode t)

;; Highlight parens
(show-paren-mode t)

;; Disable backup files
(setq make-backup-files nil)

;; Disable autosave
(setq auto-save-default nil)

;; Disable startup screen
(setq inhibit-startup-message t)

;; Move between windows with SHIFT-arrow
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; Remove blank lines at the end of file/buffer
(defun delete-trailing-blank-lines ()
  "Deletes all blank lines at the end of the file, even the last one"
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-max))
      (delete-blank-lines)
      (let ((trailnewlines (abs (skip-chars-backward "\n\t"))))
	(if (> trailnewlines 0)
	    (progn
	      (delete-char trailnewlines)))))))
(add-hook 'before-save-hook 'delete-trailing-blank-lines)

;; Enable smart wordwrap
(setq-default truncate-lines 1)
(global-visual-line-mode t)

;; Ignore patterns for grep
(eval-after-load 'grep
  '(when (boundp 'grep-find-ignored-files)
     (add-to-list 'grep-find-ignored-files "*.pyc")
     (add-to-list 'grep-find-ignored-files "*.min.css")
     (add-to-list 'grep-find-ignored-files "*.min.js")))
(eval-after-load 'grep
  '(when (boundp 'grep-find-ignored-directories)
     (add-to-list 'grep-find-ignored-directories "ckeditor")))

;; Resize windows
(global-set-key (kbd "C-<left>") 'shrink-window-horizontally)
(global-set-key (kbd "C-<right>") 'enlarge-window-horizontally)
(global-set-key (kbd "C-<up>") 'shrink-window)
(global-set-key (kbd "C-<down>") 'enlarge-window)

;; *** COMMON PACKAGES ***

;; AVY
(global-set-key (kbd "C-c a c") 'avy-goto-char)
(global-set-key (kbd "C-c a l") 'avy-goto-line)
(global-set-key (kbd "C-c a w") 'avy-goto-word-1)

;; DUPLICATE-THING
(global-set-key (kbd "M-c") 'duplicate-thing)

;; MULTIPLE-CURSORS
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)

;; WHICH-KEY
(which-key-mode t)

;; SMARTPARENS
(smartparens-global-mode 1)

;; HELM
(global-set-key (kbd "C-x C-f") 'helm-find-files)
(global-set-key (kbd "C-c h b") 'helm-mini)
(global-set-key (kbd "C-c h i") 'helm-semantic-or-imenu)
(global-set-key (kbd "C-c h m") 'helm-man-woman)
(global-set-key (kbd "C-c h f") 'helm-find)
(global-set-key (kbd "C-c h l") 'helm-locate)
(global-set-key (kbd "C-c h o") 'helm-occur)
(global-set-key (kbd "C-c h h") 'helm-apropos)
(global-set-key (kbd "C-c h a") 'helm-ag)

(setq helm-buffers-fuzzy-matching t
      helm-recentf-fuzzy-match    t
      helm-semantic-fuzzy-math    t
      helm-imenu-fuzzy-match      t
      helm-locate-fuzzy-match     t)

(helm-mode 1)
(helm-projectile-on)

;; PROJECTILE
(projectile-global-mode +1)
(defun projectile-initialize-virtual-environment ()
  "Set virtialenv version matching project name. Version must be already installed."
  (venv-workon (projectile-project-name)))
(add-hook 'projectile-switch-project-hook 'projectile-initialize-virtual-environment)

;; POWERLINE
(powerline-center-theme)

;; NLINUM
(global-nlinum-mode 1)

;; MAGIT
(global-set-key (kbd "C-c m s") 'magit-status)

;; COMPANY
(add-hook 'after-init-hook 'global-company-mode)
(eval-after-load "company"
  '(progn
     (add-to-list 'company-backends 'company-anaconda)
     (add-to-list 'company-backends 'company-tern)
     (add-to-list 'company-backends 'company-web-html)))
(company-quickhelp-mode 1)

;; *** MODES ***

;; VIRTUALENVIRONMENTWRAPPER
(venv-initialize-interactive-shells) ;; if you want interactive shell support
(venv-initialize-eshell) ;; if you want eshell support
(setq venv-location "~/.virtualenvs/")

;; PYTHON MODE (anaconda, highlight symbol, yasnippet, virtualenvwrapper, pylookup)
(defun customize-python-mode ()
  (anaconda-mode t)
  (flycheck-mode t)  
  (eldoc-mode t)
  (yas-minor-mode t)
  (yas-reload-all)
  (highlight-symbol-mode t)
  (highlight-symbol-nav-mode t))

(defun customize-keybindings ()
  (local-set-key (kbd "C-M-i") 'anaconda-mode-complete)
  (local-set-key (kbd "M-.") 'anaconda-mode-goto-definitions)
  (local-set-key (kbd "M-,") 'anaconda-nav-pop-marker)
  (local-set-key (kbd "M-?") 'anaconda-mode-view-doc)
  (local-set-key (kbd "M-r") 'anaconda-mode-usages))

(add-hook 'python-mode-hook 'customize-python-mode)
(add-hook 'python-mode-hook 'customize-keybindings)

;; WEB-MODE (Emmet)
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))

(setq web-mode-enable-current-element-highlight t)

(defun customize-web-mode-hook()
  "Hooks for Web mode."
  (setq web-mode-markup-indent-offset 4)
  (setq web-mode-css-indent-offset 4)
  (setq web-mode-code-indent-offset 4)
  (emmet-mode t))
(add-hook 'web-mode-hook 'customize-web-mode-hook)

;; CSS-MODE
(defun customize-css-mode-hook()
  "Hooks for CSS mode."
  (yas-minor-mode t)
  (yas-reload-all)
  (emmet-mode t))
(add-hook 'css-mode-hook 'customize-css-mode-hook)

;; JAVASCRIPT
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-hook 'js2-mode-hook
	  (lambda ()
	    (yas-minor-mode t)
	    (yas-reload-all)
	    (tern-mode t)
	    (highlight-symbol-mode t)
	    (highlight-symbol-nav-mode t)))

;; AUTO CUSTOMIZATION
;; (custom-set-variables
;;  ;; custom-set-variables was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  '(custom-enabled-themes (quote (wombat)))
;;  '(package-selected-packages
;;    (quote
;;     (yasnippet which-key web-mode virtualenvwrapper tern-auto-complete powerline nlinum neotree magit json-mode highlight-symbol helm-projectile helm-ag flycheck emmet-mode coffee-mode ac-js2 ac-anaconda))))
;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (wombat)))
 '(show-paren-mode t)
 '(tool-bar-mode nil))